package model;
import java.util.Comparator;

public class Sortwaitingplaces implements Comparator<Waitingplaces>{

	@Override
	public int compare(Waitingplaces o1, Waitingplaces o2) {
		Integer a = new Integer (o1.getPlace()); 
		Integer b = new Integer (o2.getPlace()); 		
		return a-b; 
	}
} 