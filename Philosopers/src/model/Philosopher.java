package model;
public class Philosopher {

	public static String NAME = "name";
	public static String NUMMER = "nummer";
	public static String ISHUNGRY = "ishungry"; 

	private String name;
	private String nummer;
	private String ishungry;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNummer() {
		return nummer;
	}
	public void setNummer(String nummer) {
		this.nummer = nummer;
	}
	public String getIshungry() {
		return ishungry;
	}
	public void setIshungry(String ishungry) {
		this.ishungry = ishungry;
	}

	public Philosopher(String name, String nummer, String ishungry) {
		this.name = new String(name);
		this.nummer = new String(nummer);
		this.ishungry = new String(ishungry);
	}

	// TO CHECK IF SOMETHING HAS CHANGED
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Philosopher) {
			Philosopher b = (Philosopher) obj;
			if (this.name.compareTo(b.getName()) == 0) {
				return true;
			}
		}
		return false;
	}
}