package model;

public class Waitingplaces {
	public static String PLACE = "place";
	public static String STATUS = "status";
	
	private String place;
	private String status;
	
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Waitingplaces (String place, String status) {
		this.place = new String(place); 
		this.status = new String(status);
	}
	
	/*
	 * To Check, if the LIST HAS CHANGED. 
	 */
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Waitingplaces) {
			Waitingplaces b = (Waitingplaces) obj;
			if (this.place.compareTo(b.getPlace()) == 0) {
				return true;
			}
		}
		return false;
	}
}