package factories;
import java.util.List;
import model.Waitingplaces;

public interface InterfaceTable {
	public static final String TABLELIST = "src/waitingTableList.csv";
	public List<Waitingplaces> getWaiter(String pathTwo);
	public void saveTableplace(Waitingplaces taplepl, String pathTwo);
	void removeWatingPlace(Waitingplaces ttppll, String pathTwo);
	void saveAlltableplaces(List<Waitingplaces> taplepl, String pathTwo);
}