package factories;
import java.util.List;

import model.Philosopher;

public interface InterfacePhilo {
	public static final String PHILOSOPHERLIST = "src/philosopherlist.csv";
	public void removePhilospher(Philosopher philo, String path);
	public void saveAll(List<Philosopher> philo, String path);
	public List<Philosopher> getAll(String path);
	public void savePhilomen(Philosopher philo, String path); 
}
