package factories;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import controller.Controller;
import model.Waitingplaces;

public class Warteliste  implements InterfaceTable {
		private static final String SPLITTER = ";";
		private List<Waitingplaces> waitingplacelist = new ArrayList<>();
		public Controller controller;
		public Warteliste(Controller c) {
			this.controller = c;
		}
		
		public List<Waitingplaces> add(String tableNr, String status) {
			Waitingplaces tapl = new Waitingplaces(tableNr, status);
			waitingplacelist.add(tapl);
			return waitingplacelist;	
		}

		public Waitingplaces watingTable(String line, String splitter) {
			String [] result = line.split(";|\r|\\.");
			return new Waitingplaces(result[0], // TableNr.
					result[1]);  // ishungri
		}

		@Override
		public List<Waitingplaces> getWaiter(String pathTwo) {
			List<Waitingplaces> tapllpl = new ArrayList<Waitingplaces>();		 
			BufferedReader br;	 
			try {
				br = new BufferedReader(new FileReader(pathTwo));
				String line;
				while ((line = br.readLine()) != null) {
					String[] fields = line.split(SPLITTER, -1);
					Waitingplaces tplaw = new Waitingplaces(fields[0], fields[1]);
					tapllpl.add(tplaw);
				}
			} catch (FileNotFoundException ex) {
			} catch (IOException ex) {
			}
			return tapllpl;
		}
		
		@Override
		public void saveTableplace(Waitingplaces taplepl, String path) {
			try {
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(path, true)));
				out.println(serializeTablePlacesToData(taplepl));
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void saveAlltableplaces(List<Waitingplaces> taplepl, String pathTwo) {
			try {
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(pathTwo, true)));
				Iterator<Waitingplaces> it = taplepl.iterator();
				while (it.hasNext()) {
					out.println(serializeTablePlacesToData(it.next()));
				}
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public String serializeTablePlacesToData(Waitingplaces phlon) {
			return phlon.getPlace() + SPLITTER + phlon.getStatus();
		}

		@Override
		public void removeWatingPlace(Waitingplaces ttppll, String pathTwo) {

			List<Waitingplaces> all = getWaiter(pathTwo);
			Iterator<Waitingplaces> it = all.iterator();
			while (it.hasNext()) {
				Waitingplaces b = it.next();
				if (b.equals(ttppll)) {
					all.remove(ttppll);
					break;
				}
			}
			try {
				Files.delete(Paths.get(pathTwo));
			} catch (IOException e) {
				e.printStackTrace();
			}
			saveAlltableplaces(all, pathTwo);
		}
	}