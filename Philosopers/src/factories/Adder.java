package factories;

import controller.Controller;
import model.Philosopher;
import model.Waitingplaces;

public class Adder {
	private Controller controller;
	private String addName;
	private String addNummer;
	private String addIsHungriy;
	private String addTablenr;
	private String addTabelstatus; 
	private boolean t1 = true; 
	private boolean t2 = true; 
	private boolean t3 = true; 
	private boolean t4 = true; 
	private boolean t5 = true; 
	public int i1, i2, i3, i4, i5, i6, i7, i8, i9 = 0;
	
	public Adder(Controller c) {
		this.controller = c;
	}

	public void addNewPhilo() { 
		addName = controller.makeString(); 
		addNummer =controller.makePhiloNumber(); 
		addIsHungriy = controller.hungry(); 
		addConstructePhilo();
	}
	
	private void addConstructePhilo() {
		Philosopher addedphilo = new Philosopher(addName, addNummer, addIsHungriy);
		controller.addNewphilo(addedphilo);
	}
	
	public void addaWaitingpalces1() { 
		addTablenr = "1"; 
		addTabelstatus = "1 - occupied";
		Waitingplaces standardwaitingpace = new Waitingplaces(addTablenr,addTabelstatus);
		controller.addNewwaitingplace(standardwaitingpace);
	}
	public void addaWaitingpalces2() { 
		addTablenr ="2"; 
		addTabelstatus = "2 - occupied";
		Waitingplaces standardwaitingpace = new Waitingplaces(addTablenr,addTabelstatus);
		controller.addNewwaitingplace(standardwaitingpace);
	}
	public void addaWaitingpalces3() { 
		addTablenr = "3"; 
		addTabelstatus = "3 - occupied";
		Waitingplaces standardwaitingpace = new Waitingplaces(addTablenr,addTabelstatus);
		controller.addNewwaitingplace(standardwaitingpace);
	}
	public void addaWaitingpalces4() { 
		addTablenr = "4"; 
		addTabelstatus = "4 - occupied";
		Waitingplaces standardwaitingpace = new Waitingplaces(addTablenr,addTabelstatus);
		controller.addNewwaitingplace(standardwaitingpace);
	}
	public void addaWaitingpalces5() { 
		addTablenr = "5";
		addTabelstatus = "5 - occupied";
		Waitingplaces standardwaitingpace = new Waitingplaces(addTablenr,addTabelstatus);
		controller.addNewwaitingplace(standardwaitingpace);
	}
	
	public void replaceWaitingPlace1() { 
		addTablenr = "1"; 
		addTabelstatus = "1 - free";
		Waitingplaces standardwaitingpace = new Waitingplaces(addTablenr,addTabelstatus);
		controller.addNewwaitingplace(standardwaitingpace);
	}
	public void replaceWaitingPlace2() { 
		addTablenr = "2"; 
		addTabelstatus = "2 - free";
		Waitingplaces standardwaitingpace = new Waitingplaces(addTablenr,addTabelstatus);
		controller.addNewwaitingplace(standardwaitingpace);
	}
	public void replaceWaitingPlace3() { 
		addTablenr = "3"; 
		addTabelstatus = "3 - free";
		Waitingplaces standardwaitingpace = new Waitingplaces(addTablenr,addTabelstatus);
		controller.addNewwaitingplace(standardwaitingpace);
	}
	public void replaceWaitingPlace4() { 
		addTablenr = "4"; 
		addTabelstatus = "4 - free";
		Waitingplaces standardwaitingpace = new Waitingplaces(addTablenr,addTabelstatus);
		controller.addNewwaitingplace(standardwaitingpace);
	}
	public void replaceWaitingPlace5() { 
		addTablenr = "5"; 
		addTabelstatus = "5 - free";
		Waitingplaces standardwaitingpace = new Waitingplaces(addTablenr,addTabelstatus);
		controller.addNewwaitingplace(standardwaitingpace);
	}	

	public void getStatus() {
	System.out.print(i1 );System.out.print(" " + t1 + " " );
	System.out.print(i2 );System.out.print(" " + t2 + " ");
	System.out.print(i3 );System.out.print( " " + t3 + " " );
	System.out.print(i4 );System.out.print( " " + t4 + " ");
	System.out.print(i5 );System.out.println( " " + t5 + " " );
}

	public void reset () {
		controller.clearDisplayedList();
		controller.removeSelectedtableplace5();
		controller.removeSelectedtableplace4();
		controller.removeSelectedtableplace3();
		controller.removeSelectedtableplace2();
		controller.removeSelectedtableplace1();
		replaceWaitingPlace1();
		replaceWaitingPlace2();
		replaceWaitingPlace3();
		replaceWaitingPlace4();
		replaceWaitingPlace5();
		t1 = true; 
		t2 = true; 
		t3 = true; 
		t4 = true; 
		t5 = true; 
		i1= 0;
		i2= 0;
		i3= 0;
		i4= 0;
		i5 = 0;
		i7 = 0;
		i9= 0; 
		System.out.println("ALLES AUF NULL");
	}
	
	// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
	public void checkTree() {
		if (i7 >= 3) {
			System.out.println("3x gegessen Eating3 tree" + i7);
			i7=0; 
		} i7++;
		
		if ((t1 == true) && (t5 == true) && (t2 == true))  {
			addaWaitingpalces1();
			controller.clearDisplayedList();
			controller.removeSelectedtableplace1();
			controller.clearDisplayedList();
			controller.removeFirstPhilo();
			i1 = 0; 
			t1 = false;
		} else i1++;  
		if (i1 >= 3)  {
			t1 = true; 
			controller.removeSelectedtableplace1();
			replaceWaitingPlace1();
			controller.clearDisplayedList();
		}
		
		if ((t2 == true) && (t1 == true) && (t3 == true))  {
			addaWaitingpalces2();
			controller.clearDisplayedList();
			controller.removeSelectedtableplace2();
			controller.clearDisplayedList();
			controller.removeFirstPhilo();
			i2 = 0; 
			t2 = false;
		} else i2++;  
		if (i2 >= 3)  {
			t2 = true; 
			controller.removeSelectedtableplace2();
			replaceWaitingPlace2();
			controller.clearDisplayedList();
		}
		
		if ((t3 == true) && (t2 == true) && (t4 == true))  {
			addaWaitingpalces3();
			controller.clearDisplayedList();
			controller.removeSelectedtableplace3();
			controller.clearDisplayedList();
			controller.removeFirstPhilo();
			i3 = 0; 
			t3 = false;
		} else i3++;  
		if (i3 >= 3)  {
			t3 = true; 
			controller.removeSelectedtableplace3();
			replaceWaitingPlace3();
			controller.clearDisplayedList();
		}
		
		if ((t4 == true) && (t3 == true) && (t5 == true))  {
			addaWaitingpalces4();
			controller.clearDisplayedList();
			controller.removeSelectedtableplace4();
			controller.clearDisplayedList();
			controller.removeFirstPhilo();
			i4 = 0; 
			t4 = false;
		} else i4++;  
		if (i4 >= 3)  {
			t4 = true; 
			controller.removeSelectedtableplace4();
			controller.clearDisplayedList();
			replaceWaitingPlace4();
			controller.clearDisplayedList();
		}
		
		if ((t5 == true) && (t4 == true) && (t1 == true))  {
			addaWaitingpalces5();
			controller.clearDisplayedList();
			controller.removeSelectedtableplace5();
			controller.clearDisplayedList();
			controller.removeFirstPhilo();
			i5 = 0; 
			t5 = false;
		} else i5++;  
		if (i5 >= 3)  {
			t5 = true; 
			controller.removeSelectedtableplace5();
			controller.clearDisplayedList();
			replaceWaitingPlace5();
			controller.clearDisplayedList();
		}
	addNewPhilo(); 
	getStatus();
		return;
	}	

	// OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
	public void checkTwo() {
		if (i9 >= 2) {
			System.out.println("2x gegessen Eating2" + i9);
			i9=0; 
		} i9++;

		if ((t1 == true) && (t5 == true) && (t2 == true))  {
			addaWaitingpalces1();
			controller.clearDisplayedList();
			controller.removeSelectedtableplace1();
			controller.clearDisplayedList();
			controller.removeFirstPhilo();
			i1 = 0; 
			t1 = false;
		} else i1++;  
		if (i1 >= 2)  {
			t1 = true; 
			controller.removeSelectedtableplace1();
			replaceWaitingPlace1();
			controller.clearDisplayedList();
		}


		if ((t2 == true) && (t1 == true) && (t3 == true))  {
			addaWaitingpalces2();
			controller.clearDisplayedList();
			controller.removeSelectedtableplace2();
			controller.clearDisplayedList();
			controller.removeFirstPhilo();
			i2 = 0; 
			t2 = false;
		} else i2++;  
		if (i2 >= 2)  {
			t2 = true; 
			controller.removeSelectedtableplace2();
			replaceWaitingPlace2();
			controller.clearDisplayedList();
		}

		if ((t3 == true) && (t2 == true) && (t4 == true))  {
			addaWaitingpalces3();
			controller.clearDisplayedList();
			controller.removeSelectedtableplace3();
			controller.clearDisplayedList();
			controller.removeFirstPhilo();
			i3 = 0; 
			t3 = false;
		} else i3++;  
		if (i3 >= 2)  {
			t3 = true; 
			controller.removeSelectedtableplace3();
			replaceWaitingPlace3();
			controller.clearDisplayedList();
		}

		if ((t4 == true) && (t3 == true) && (t5 == true))  {
			addaWaitingpalces4();
			controller.clearDisplayedList();

			controller.removeSelectedtableplace4();
			controller.clearDisplayedList();

			controller.removeFirstPhilo();
			i4 = 0; 
			t4 = false;
		} else i4++;  
		if (i4 >= 2)  {
			t4 = true; 
			controller.removeSelectedtableplace4();
			controller.clearDisplayedList();
			replaceWaitingPlace4();
			controller.clearDisplayedList();
		}

		if ((t5 == true) && (t4 == true) && (t1 == true))  {
			addaWaitingpalces5();
			controller.clearDisplayedList();
			controller.removeSelectedtableplace5();
			controller.clearDisplayedList();
			controller.removeFirstPhilo();
			i5 = 0; 
			t5 = false;
		} else i5++;  
		if (i5>= 2)  {
			t5 = true; 
			controller.removeSelectedtableplace5();
			controller.clearDisplayedList();
			replaceWaitingPlace5();
			controller.clearDisplayedList();
		}
		addNewPhilo(); 
		getStatus();
		return;
	}
}