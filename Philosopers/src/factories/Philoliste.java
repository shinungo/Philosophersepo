package factories;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import model.Philosopher;

	public class Philoliste implements InterfacePhilo{
		private static final String SPLITTER = ";";
		private List<Philosopher> names = new ArrayList<>();
		public List<Philosopher> getNames() {
			return names;
		}
		public List<Philosopher> add(String name, String nummer, String ishungry) {
			Philosopher phso = new Philosopher(name, nummer, ishungry);
			names.add(phso);
			return names;	
		}

		public Philosopher csvBabynamen(String line, String splitter) {
			String [] result = line.split(";|\r|\\.");
			return new Philosopher(result[0], // Name
					result[1], // nummer
					result[2]); // ishungri
		}


		@Override
		public List<Philosopher> getAll(String path) {
			List<Philosopher> philos = new ArrayList<Philosopher>();		 
			BufferedReader br;		 
			try {
				br = new BufferedReader(new FileReader(path));
				String line;
				while ((line = br.readLine()) != null) {
					String[] fields = line.split(SPLITTER, -1);

					Philosopher philoss = new Philosopher(fields[0], fields[1], fields[2]);
					philos.add(philoss);
				}
			} catch (FileNotFoundException ex) {
			} catch (IOException ex) {
			}
			return philos;
		}


		@Override
		public void savePhilomen(Philosopher philo, String path) {
			try {
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(path, true)));
				out.println(serializeBabynameToData(philo));
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void saveAll(List<Philosopher> philo, String path) {
			try {
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(path, true)));
				Iterator<Philosopher> it = philo.iterator();
				while (it.hasNext()) {
					out.println(serializeBabynameToData(it.next()));
				}
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public String serializeBabynameToData(Philosopher phlon) {
			return phlon.getName() + SPLITTER + phlon.getNummer() + SPLITTER + phlon.getIshungry();

		}

		@Override
		public void removePhilospher(Philosopher philo, String path1) {

			List<Philosopher> all = getAll(path1);
			Iterator<Philosopher> it = all.iterator();
			while (it.hasNext()) {
				Philosopher b = it.next();
				if (b.equals(philo)) {
					all.remove(philo);
					break;
				}
			}
			try {
				Files.delete(Paths.get(path1));
			} catch (IOException e) {
				e.printStackTrace();
			}
			saveAll(all, path1);
		}
		
//		// Kompletter Suchmechanismus 
//		public List<Philosopher> search(Searchparameter params, String path) {
//	      List<Philosopher> all = getAll(path);
//	            
//	      //Filtersuche aufgrund der Auswahl im 
//	      return all
//	      	.stream()
//	          .collect(Collectors.toList());                
//		}
	}