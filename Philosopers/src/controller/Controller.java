package controller;

import java.io.IOException;
import factories.Adder;
import factories.InterfacePhilo;
import factories.InterfaceTable;
import factories.Philoliste;
import factories.Randomthing;
import factories.Warteliste;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Philosopher;
import model.Sortwaitingplaces;
import model.Waitingplaces;
import views.InformationLabels;
import views.ButtonsBox;
import views.Philosophertableview;
import views.WaitingtableView;

public class Controller extends Application {
	private ObservableList<Waitingplaces> waitinglistas;
	private ObservableList<Philosopher> philosopherlistas;
	private InterfaceTable interfacetable; 
	private InterfacePhilo interfacephilo; 
	public WaitingtableView waitingtableview; 
	public Warteliste warteliste; 
	private String watingtableepath = null;
	public Philosophertableview philosophertableview; 
	public Philoliste philoiste; 	
	private String currentFilePath = null; 
	private VBox localwaitingtable; 
	private VBox localphilophertable; 
	private InformationLabels info;
	private Stage stage; 
	private Scene scene;
	private HBox horizontalbox, mainbox, spacer;
	private VBox verticalbox; 
	private Label top;
	private Label bot;
	private ButtonsBox nextbutton; 
	private HBox nextbotton; 
	public String counterNrString=""; 
	public int iinter, counte = 0; 
	public Adder adder;
	private Randomthing random; 
	public String randomSigns="";
	public int wundex = 0; 
	public String free; 
	private Sortwaitingplaces sortedList; 

	public Controller() {
		this.random = new Randomthing();
		this.interfacetable = new Warteliste(null);	
		this.interfacephilo = new Philoliste();
		this.waitinglistas = FXCollections.observableArrayList(interfacetable.getWaiter(getWatingtable()));
		this.sortedList = new Sortwaitingplaces() ;
		waitinglistas.sort(sortedList);
		this.waitingtableview = new WaitingtableView(this);
		localwaitingtable = waitingtableview.makeGridWaitbngView();
		this.philosopherlistas = FXCollections.observableArrayList(interfacephilo.getAll(getDefaultFile()));
		this.philosophertableview = new Philosophertableview(this); 
		localphilophertable = philosophertableview.makeGridVBoxView();
		this.info = new InformationLabels();
		bot = info.bootemtxt();
		top = info.toptext();
		this.nextbutton = new ButtonsBox(this);
		nextbotton = nextbutton.createNextHandler();
		this.adder = new Adder(this);
	}

	//  ADDING Philosophers 
	public void addNewphilo(Philosopher newPhilo) {
		philosopherlistas.add(newPhilo);
		interfacephilo.savePhilomen(newPhilo, getDefaultFile());
	}
	public ObservableList<Philosopher> getPhilonamen() {
		return philosopherlistas;
	}
	// Makes a Random String for PhiloName
	public String makeString() {
		randomSigns = random.makeThing();
		return randomSigns;
	}
	public String makePhiloNumber() {
		iinter++;
		counterNrString = new Integer(iinter).toString();
		return counterNrString;
	}
	public String hungry() {
		return "hungry";
	}

	//  Adding for Table Place
	public void addNewwaitingplace(Waitingplaces newWaitingplace) {
		waitinglistas.add(newWaitingplace);
		interfacetable.saveTableplace(newWaitingplace, getWatingtable());
	}

	public ObservableList<Waitingplaces> getWaitingPlace() {
		return waitinglistas;
	}

	public void clearDisplayedList() {
		waitinglistas.clear();
		waitinglistas.addAll(interfacetable.getWaiter(getWatingtable()));
		waitinglistas.sort(sortedList);
	}	

	public void removeSelectedtableplace1() {
		Waitingplaces selctedWaitingplace = waitinglistas.get(0);
		interfacetable.removeWatingPlace(selctedWaitingplace, getWatingtable());
		waitinglistas.remove(selctedWaitingplace);
	}

	public void removeSelectedtableplace2() {
		Waitingplaces selctedWaitingplace = waitinglistas.get(1);
		interfacetable.removeWatingPlace(selctedWaitingplace, getWatingtable());
		waitinglistas.remove(selctedWaitingplace);
	}
	public void removeSelectedtableplace3() {
		Waitingplaces selctedWaitingplace = waitinglistas.get(2);
		interfacetable.removeWatingPlace(selctedWaitingplace, getWatingtable());
		waitinglistas.remove(selctedWaitingplace);
	}
	public void removeSelectedtableplace4() {
		Waitingplaces selctedWaitingplace = waitinglistas.get(3);
		interfacetable.removeWatingPlace(selctedWaitingplace, getWatingtable());
		waitinglistas.remove(selctedWaitingplace);
	}
	public void removeSelectedtableplace5() {
		Waitingplaces selctedWaitingplace = waitinglistas.get(4);
		interfacetable.removeWatingPlace(selctedWaitingplace, getWatingtable());
		waitinglistas.remove(selctedWaitingplace);
	}

	public void makeaPhilo() {
		adder.addNewPhilo();   
	}

	// For Manual Removements
	public void removeSelectedPhilos() {
		Philosopher selectedToremove = philosophertableview.getSelected();
		interfacephilo.removePhilospher(selectedToremove, getDefaultFile());
		philosopherlistas.remove(selectedToremove);
		getPhilonamen();
	}

	public void removeFirstPhilo() {
		Philosopher selectedToremove =  philosopherlistas.get(0); 
		interfacephilo.removePhilospher(selectedToremove, getDefaultFile());
		philosopherlistas.remove(selectedToremove);
		getPhilonamen();
	}

	// 3 x eat check counter-check AFTER boolean 
	public void checkTree() {
		adder.checkTree();
	}

	// 2 x eat check counter-check AFTER boolean 	
	public void checkTwo() {
		adder.checkTwo();
	}

	public void reset() {
		adder.reset();
	}	

	// Creates Table with Wating Places 
	private String getWatingtable() {
		if (watingtableepath == null) {
			return InterfaceTable.TABLELIST;
		} else {
			return watingtableepath;
		}
	}

	private String getDefaultFile() {
		if (currentFilePath == null) {
			return InterfacePhilo.PHILOSOPHERLIST;
		} else {
			return currentFilePath;
		}
	}

	public ObservableList<Waitingplaces> getWaitinglist() {
		return waitinglistas;
	}

	public ObservableList<Philosopher> getPhilos() {
		return philosopherlistas;
	}

	public static void startingMachine(String[] args) {
		Application.launch(args);
	}

	public void start(Stage stage) throws Exception {
		this.stage = stage;
		showStandardView();
	}

	public void showStandardView() throws IOException {
		horizontalbox = new HBox(); 
		mainbox = new HBox(); 
		verticalbox = new VBox(); 
		spacer = new HBox(); 
		spacer.setPrefWidth(22); 
		horizontalbox.getChildren().addAll(localphilophertable, localwaitingtable);
		verticalbox.getChildren().addAll(top, horizontalbox, nextbotton, bot); 
		mainbox.getChildren().addAll(spacer, verticalbox);
		scene = new Scene(mainbox, 880, 650);
		stage.setScene(scene);
		stage.setTitle("PhilosopherSolution");
		stage.show();
	}
}