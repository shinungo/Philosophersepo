package views;

import controller.Controller;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.VBox;
import model.Waitingplaces;

public class WaitingtableView {
	
		private Controller controller; 

		public WaitingtableView(Controller controller) {
			this.controller = controller;
		}
		
		private TableView<Waitingplaces> table = new TableView<Waitingplaces>();
		VBox vBox = new VBox();
		TableColumn<Waitingplaces, String> columnF1 = createTableColumn("Place", Waitingplaces.PLACE);
		TableColumn<Waitingplaces, String> columnF2 = createTableColumn("Status", Waitingplaces.STATUS);
		
			@SuppressWarnings("unchecked")
			public VBox makeGridWaitbngView() {
				columnF1.setMinWidth(30);
				columnF2.setMinWidth(199);
				table.setItems(controller.getWaitinglist());
				table.getColumns().addAll(columnF1, columnF2);
				vBox.getChildren().addAll(table);
				return vBox;
			}
				
			private TableColumn<Waitingplaces, String> createTableColumn(String name, String p) {
				TableColumn<Waitingplaces, String> tc = new TableColumn<Waitingplaces, String>(name);
				tc.setCellValueFactory(new PropertyValueFactory<Waitingplaces, String>(p));

				tc.setCellFactory(TextFieldTableCell.forTableColumn());
				tc.setOnEditCommit(new EventHandler<CellEditEvent<Waitingplaces, String>>() {
					@Override
					public void handle(CellEditEvent<Waitingplaces, String> t) {
						((Waitingplaces) t.getTableView().getItems().get(t.getTablePosition().getRow())).setPlace(t.getNewValue());
					}
				});
				return tc;
			}
			public Waitingplaces getSelectedPlace() {
				return table.getSelectionModel().getSelectedItem();
			}
	}