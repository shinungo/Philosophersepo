package views;

import controller.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

public class ButtonsBox {
	private Controller controller;
	Button addButton = new Button("Add a Philo");
	Button removeButton = new Button("delete selecte Philo");
	Button buttonEatTree = new Button("Eat_in_3 ");	
	Button buttonEatTwoo = new Button("Eat_in_2 ");	
	Button resetButton = new Button ("reset");
	
	public ButtonsBox(Controller controller) {
		this.controller = controller;
	}

	// Add A Philo Button 
	public EventHandler<ActionEvent> makeaPhilo() {
		EventHandler<ActionEvent> clearByPush = event -> {
			controller.makeaPhilo();
		};
		return clearByPush;
	}
	
	public EventHandler<ActionEvent> removeSelectedPhilos() {
		EventHandler<ActionEvent> clearByPush = event -> {
			controller.removeSelectedPhilos();
		};
		return clearByPush;
	}	
	
	// Button Eatin3 
	public EventHandler<ActionEvent> makeNext() {
		EventHandler<ActionEvent> clearByPush = event -> {
			controller.checkTree();
		};
		return clearByPush;
	}
	
		public EventHandler<ActionEvent> checkTwo() {
			EventHandler<ActionEvent> clearByPush = event -> {
				controller.checkTwo();
			};
			return clearByPush;
		}
		
		public EventHandler<ActionEvent> reset() {
			EventHandler<ActionEvent> clearByPush = event -> {
				controller.reset();
			};
			return clearByPush;
		}

	public HBox createNextHandler() {
		buttonEatTree.setOnAction(makeNext());
		addButton.setOnAction(makeaPhilo());
		removeButton.setOnAction(removeSelectedPhilos());
		buttonEatTwoo.setOnAction(checkTwo()); 
		resetButton.setOnAction(reset());
		HBox hbox = new HBox(7, addButton ,  removeButton , buttonEatTree ,buttonEatTwoo, resetButton );
		return hbox;
	}
}