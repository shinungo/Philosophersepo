package views;
import controller.Controller;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.VBox;
import model.Philosopher;


public class Philosophertableview {

	private Controller controller; 
	public Philosophertableview(Controller controller) {
		this.controller = controller;
	}
	
	private TableView<Philosopher> table = new TableView<Philosopher>();
	VBox vBox = new VBox();
	TableColumn<Philosopher, String> columnF1 = createTableColumn("Namen", Philosopher.NAME);
	TableColumn<Philosopher, String> columnF2 = createTableColumn("Nummer", Philosopher.NUMMER);
	TableColumn<Philosopher, String> columnF3 = createTableColumn("IsHungry", Philosopher.ISHUNGRY);
	
		@SuppressWarnings("unchecked")
		public VBox makeGridVBoxView() {
			columnF1.setMinWidth(132);
			columnF2.setMinWidth(132);
			columnF3.setMinWidth(132);
			table.setItems(controller.getPhilos());
			table.setEditable(true);
			table.getColumns().addAll(columnF1, columnF2, columnF3);
			vBox.getChildren().addAll(table);
			return vBox;
		}

		private TableColumn<Philosopher, String> createTableColumn(String name, String p) {
			TableColumn<Philosopher, String> tc = new TableColumn<Philosopher, String>(name);
			tc.setCellValueFactory(new PropertyValueFactory<Philosopher, String>(p));

			tc.setCellFactory(TextFieldTableCell.forTableColumn());
			tc.setOnEditCommit(new EventHandler<CellEditEvent<Philosopher, String>>() {
				@Override
				public void handle(CellEditEvent<Philosopher, String> t) {
					((Philosopher) t.getTableView().getItems().get(t.getTablePosition().getRow())).setName(t.getNewValue());
				}
			});
			return tc;
		}
		public Philosopher getSelected() {
			return table.getSelectionModel().getSelectedItem();
		}
}